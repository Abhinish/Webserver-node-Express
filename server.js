const express = require('express');
const hbs = require('hbs');
const fs = require('fs');
const port = process.env.PORT || 3000;
var app = express();


hbs.registerPartials(__dirname+'/views/partial');
app.set('view engine','hbs');
hbs.registerHelper('getcurrentYear',() =>{
   return new Date().getFullYear();
});
hbs.registerHelper('screamIt',(text) => {
   return text.toUpperCase();
});


app.use((req,res,next) => {
   var now = new Date().toString();
   var log = `${now}:${req.method} ${req.url}`;

   console.log(log);
    fs.appendFile('server.log',log+'\n',(err) => {
        if(err){
        console.log('Unable to open the file');
        }else {
            console.log('File Updated');
        }
    });
    next()
});

//block all the other pages from executing
// app.use((req,res,next) => {
// res.render('maintenance.hbs',);
//
// });

app.use(express.static(__dirname+'/public'));

app.get('/',(req,res) =>  {

res.render('home.hbs',{
    pageTitle:'Home Page',
    userName:'Abhinish Raj',
    welcomeMessage:'Welcome to Home page'
});

});
app.get('/log',(req,res) => {
    var allLogs = fs.readFileSync('server.log');
    res.send(allLogs.toString());

});
app.get('/abhinish',(req,res) =>  {

    res.send({
    Name: 'Siddu',
    Work: 'work is good',
    song: 'rock song',
    list: ['metaphore','metaphore','metaphore']
});
});

app.get('/projects',(req,res) => {
   res.render('projects.hbs',{
       pageTitle:'Project Page',

   });
});

app.get('/bad',(req,res) =>  {
    res.send({
    errorMessage : 'Unable to handle the request'
});
});

app.get('/about',(req,res) =>  {
    res.render('about.hbs',{
        pageTitle:'About Page'

});
});

app.listen(port,() => {
    console.log(`Server started at ${port}`)
});